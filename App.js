import React from 'react';
import { StyleSheet, Text, View } from 'react-native'
import { Font } from 'expo'
import Payment from './src/screens/payment'

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { fontLoaded: false };
  }

  async componentDidMount() {
    try {
      await Font.loadAsync({
        'SourceSansPro-Semibold': require('./assets/fonts/SourceSansPro-Semibold.ttf'),
        'SourceSansPro-Regular': require('./assets/fonts/SourceSansPro-Regular.ttf')
      });
      this.setState({ fontLoaded: true });
      console.log('fonts are loaded');
    } catch (error) {
      console.log(error);
    }
  }

  render() {
    if(this.state.fontLoaded) {
      return (
        <View style={styles.container}>
          <Payment />
        </View>
      )
    }
    return (
      <View style={styles.container}>
        <Text>Loading app</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
