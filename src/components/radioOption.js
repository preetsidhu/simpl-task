import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'

import { ColorCodes } from '../utils/constants'

const styles = StyleSheet.create({
    OptionBox: {
        marginLeft: 4
    },
    TitleText: {
        fontWeight: 'bold',
        fontSize: 15,
        color: ColorCodes.BLACK,
        fontFamily: 'SourceSansPro-Semibold'
    },
    SecondaryText: {
        fontSize: 13, 
        color: ColorCodes.DARK_GREY,
        fontFamily: 'SourceSansPro-Regular'
    }
})


const RadioOption = (props) => {
    let { titleText, secondaryText, rupee } = props

    return (
        <View style={styles.OptionBox}>
            <Text style={styles.TitleText}>{titleText}</Text>
            <Text style={styles.SecondaryText}>
                {secondaryText}
            </Text>
        </View>
    )
}

export default RadioOption