import React, { Component } from 'react'
import { TouchableHighlight, StyleSheet, View, Text, Image } from 'react-native'
import PropTypes from 'prop-types'

import { ColorCodes } from '../utils/constants'

const defaultProps = {
  text: '',
  customOpacity: 0.6,
  customUnderlayColor: ColorCodes.PRIMARY_GREEN,
  buttonStyle: {},
  iconStyle: {},
  textStyle: {},
  containerStyle: {},
};

const propTypes = {
  text: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
  icon: PropTypes.string,
  customOpacity: PropTypes.number,
  customUnderlayColor: PropTypes.string,
  buttonStyle: View.propTypes.style,
  iconStyle: View.propTypes.style,
  textStyle: Text.propTypes.style,
  containerStyle: View.propTypes.style
};

const styles = StyleSheet.create({
  Button: {
    backgroundColor: ColorCodes.PRIMARY_GREEN,
    paddingLeft: 16,
    paddingRight: 16,
    paddingTop: 8,
    paddingBottom: 8,
    borderRadius: 4,

    shadowColor: ColorCodes.BLACK,
    shadowOffset: { width: 10, height: 10 },
    shadowOpacity: 0.1,
    shadowRadius: 2,
    elevation: 3,
  },
  Text: {
    color: ColorCodes.WHITE,
    fontSize: 15,
    textAlign: 'center'
  }
})

class TouchableHighlightButton extends Component {

  handlePress = (event) => {
    this.props.onPress(event)
  }

  render() {
    let { text, onPress, buttonStyle, icon, iconStyle, textStyle, customOpacity, containerStyle, customUnderlayColor, active } = this.props

    return (
      <View style={containerStyle}>
      <TouchableHighlight
        style={[styles.Button, buttonStyle]}
        onPress={this.handlePress}
        activeOpacity={customOpacity}
        underlayColor={active? customUnderlayColor : ColorCodes.BORDER_GREY}>
        <View>
          {icon && 
            <Image
            source={icon}
            style={iconStyle} />}
          <Text
          style={[styles.Text, textStyle]}>{text.toUpperCase()}</Text>
        </View>
      </TouchableHighlight>
      </View>
    )
  }
}

TouchableHighlightButton.propTypes = propTypes
TouchableHighlightButton.defaultProps = defaultProps

export default TouchableHighlightButton;