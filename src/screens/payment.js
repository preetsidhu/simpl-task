import React from 'react'
import { StyleSheet, View } from 'react-native'

import TouchableHighlightButton from '../components/touchableHighlightButton'
import PaymentHeader from '../containers/paymentHeader'
import PaymentInputBox from '../containers/paymentInputBox'
import PaymentOptions from '../containers/paymentOptions'
import { ColorCodes, StringConstants } from '../utils/constants'


const styles = StyleSheet.create({
    PaymentContainer: {
        flex: 1,
        flexDirection: 'column',
        alignSelf: 'stretch',
        backgroundColor: ColorCodes.LIGHT_GREY
    },
    PaymentButtonBox: {
        margin: 16
    },
    PaymentButton: {
        paddingTop: 16,
        paddingBottom: 16
    },
    PaymentButtonInactive: {
        backgroundColor: ColorCodes.BORDER_GREY
    }
})

class Payment extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            enablePayment: false,
            creditLimit: 2000,
            text: ''
        }
    }

    handleTextChange = text => {
        this.setState({
            text,
            enablePayment: Number(text) <= this.state.creditLimit
        })
    }

    handlePayment = ev => {
        ev && ev.stopPropagation()
        // this.state.enablePayment && this.props.processPayment(this.state.text)
    }

    handleChangeClick = ev => {
        ev && ev.stopPropagation()
    }

    handleCloseClick = ev => {
        ev && ev.stopPropagation()
    }

    render() {
        return (
            <View style={styles.PaymentContainer}>
                <PaymentHeader
                handleChangeClick={this.handleChangeClick} />

                <PaymentInputBox
                text={this.state.text}
                handleChange={this.handleTextChange} />

                <PaymentOptions
                amount={this.state.text}
                limit={this.state.creditLimit}
                isValid={this.state.enablePayment} />
                
                <TouchableHighlightButton
                    text={StringConstants.PAYMENT_BUTTON_TEXT}
                    onPress={this.handlePayment}
                    buttonStyle={[styles.PaymentButton, this.state.enablePayment ? {} : styles.PaymentButtonInactive]}
                    containerStyle={styles.PaymentButtonBox}
                    active={this.state.enablePayment} />
            </View>
        )
    }
}

export default Payment