export const CHANGE_BUTTON_TEXT = 'change'

export const PAYMENT_INPUT_PLACEHOLDER = 'Enter bill amount'

export const PAYMENT_OPTIONS_HEADER = 'Pay using'

export const getPaymentStatus = (isValid, amount, limit) => {
    return isValid ? '₹' + amount + ' will be charged from your account' : '₹' + limit + ' credit is not sufficient'
}

export const PAYMENT_BUTTON_TEXT = 'pay bill'