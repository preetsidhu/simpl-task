import * as ColorCodes from './colorCodes'
import * as StringConstants from './stringConstants'

export { ColorCodes, StringConstants }