import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'

import TouchableHighlightButton from '../components/touchableHighlightButton'
import { ColorCodes, StringConstants } from '../utils/constants'

const styles = StyleSheet.create({
    PaymentHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingTop: 16,
        paddingBottom: 16,
        paddingLeft: 16,
        paddingRight: 16,
        backgroundColor: ColorCodes.PRIMARY_GREEN
    },
    UserDetailsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    userDetails: {
        flexDirection: 'column',
        justifyContent: 'space-around'
    },
    ChangeButton: {
        backgroundColor: ColorCodes.WHITE
    },
    ChangeButtonText: {
        color: ColorCodes.PRIMARY_GREEN,
        fontSize: 12,
        fontWeight: 'bold',
        fontFamily: 'SourceSansPro-Semibold'
    },
    CloseContainer: {
        alignItems: 'flex-end'
    },
    CloseButton: {
        fontSize: 32, 
        color: 'white'
    },
    BaseText: {
        fontSize: 13,
        color: ColorCodes.WHITE,
        fontFamily: 'SourceSansPro-Regular'
    },
    TitleText: {
        fontSize: 18,
        color: ColorCodes.WHITE,
        fontFamily: 'SourceSansPro-Semibold'
    }
})

const PaymentHeader = (props) => {
    let { handleChangeClick } = props
    return(
        <View style={styles.PaymentHeader}>
            <View style={styles.UserDetailsContainer}>
                <View style={styles.UserDetails}>
                    <Text  style={styles.TitleText}>Roshan Sam</Text>
                    <Text  style={styles.BaseText}>9876032737</Text>
                </View>
                <TouchableHighlightButton
                    text={StringConstants.CHANGE_BUTTON_TEXT}
                    onPress={ev => handleChangeClick(ev)}
                    buttonStyle={styles.ChangeButton}
                    textStyle={styles.ChangeButtonText}
                    containerStyle={{marginLeft: 16}}
                    customUnderlayColor={ColorCodes.WHITE} />
            </View>
            <View style={styles.CloseContainer}>
                <Text style={styles.CloseButton}>&times;</Text>
            </View>
        </View>
    )
}

export default PaymentHeader