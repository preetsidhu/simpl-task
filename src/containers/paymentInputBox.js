import React, { Component } from 'react'
import { StyleSheet, View, Text, TextInput, Image } from 'react-native'

import { ColorCodes, StringConstants } from '../utils/constants'

const styles = StyleSheet.create({
    BillInputContainer: {
        flexDirection: 'row',
        alignSelf: 'stretch',
        backgroundColor: ColorCodes.PRIMARY_GREEN,
        paddingLeft: 16,
        paddingRight: 16
    },
    InputContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        alignSelf: 'stretch',
        backgroundColor: ColorCodes.TRANSPARENT_GREEN,
        marginBottom: 16
    },
    RupeeContainer: {
        marginLeft: 16,
        marginTop: 13,
        marginBottom: 13
    },
    InputBox: {
        marginLeft: 8
    },
    Input: {
        color: ColorCodes.WHITE, 
        fontSize: 24,
        minWidth: 200,
        fontFamily: 'SourceSansPro-Semibold'
    },
})

const PaymentInputBox = (props) => {
    let { text, handleChange } = props
    return (
        <View style={[styles.BillInputContainer, styles.basePadding, styles.baseBackground]}>
            <View style={styles.InputContainer}>
                <View style={styles.RupeeContainer}>
                    <Image
                    source={require('../../assets/rupee.png')}
                    fadeDuration={0}
                    style={{width: 16, height: 30}} />
                </View>
                <View style={styles.InputBox}>
                    <TextInput
                    style={styles.Input}
                    onChangeText={(text) => handleChange(text)}
                    value={text}
                    keyboardType = 'numeric'
                    placeholder={StringConstants.PAYMENT_INPUT_PLACEHOLDER}
                    placeholderTextColor={ColorCodes.BLUISH_GREY}
                    underlineColorAndroid='transparent'
                    autoFocus={true} />
                </View>
            </View>
        </View>
    )
}

export default PaymentInputBox