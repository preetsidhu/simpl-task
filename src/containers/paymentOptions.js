import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StyleSheet, View, Text } from 'react-native'

import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import RadioOption from '../components/radioOption'
import { ColorCodes, StringConstants } from '../utils/constants'

const styles = StyleSheet.create({
    OptionsHeader: {
        paddingLeft: 16,
        paddingTop: 8,
        paddingBottom: 8,
        borderBottomWidth: 1,
        borderBottomColor: ColorCodes.BORDER_GREY,
        backgroundColor: ColorCodes.WHITE,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        marginLeft: 16,
        marginRight: 16
    },
    HeaderText: {
        color: ColorCodes.DARK_GREY,
        fontSize: 13,
        fontFamily: 'SourceSansPro-Regular'
    },

    OptionsContainer: {
        flexDirection: 'column',
        backgroundColor: ColorCodes.WHITE,
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
        marginLeft: 16,
        marginRight: 16,
    },
    RadioGroup: {
        margin: 6
    },
    basePadding: {
        paddingLeft: 16,
        paddingRight: 16
    },
    baseBackground: {
        backgroundColor: ColorCodes.PRIMARY_GREEN
    },
    containerShadow: {
        shadowColor: ColorCodes.BLACK,
        shadowOffset: { width: 10, height: 10 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
        elevation: 3,
    }
})

const PaymentOptions = (props) => {
    let { amount, limit, isValid } = props
    return (
        <View>
            <View style={[styles.OptionsHeaderContainer, styles.baseBackground]}>
                <View style={[styles.OptionsHeader, styles.basePadding, styles.containerShadow]}>
                    <Text style={styles.HeaderText}>{StringConstants.PAYMENT_OPTIONS_HEADER}</Text>
                </View>
            </View>
            <View style={[styles.OptionsContainer, styles.containerShadow]}>
                <RadioGroup
                size={24}
                thickness={2}
                color={ColorCodes.PRIMARY_GREEN}
                highlightColor={ColorCodes.WHITE}
                selectedIndex={0}
                style={styles.RadioGroup}
                onSelect = {(index, value) => console.log(index + ' ' + value)}>

                    <RadioButton 
                    value={'SIMPL'}> 
                        <RadioOption
                        titleText={'Simpl: ₹2000 available'}
                        secondaryText={amount ? StringConstants.getPaymentStatus(isValid, Number(amount), limit) : ''} />
                    </RadioButton>

                    <RadioButton 
                    value={'UPI'}> 
                        <RadioOption
                        titleText={'UPI'}
                        secondaryText={'A payment request will be sent to your VPA'} />
                    </RadioButton>

                </RadioGroup>
            </View>
        </View>
    )
}

export default PaymentOptions